C = gcc
CFLAGS = -O3 -Wall -Wextra -ggdb

PREFIX = /usr/local

all: ppdevd

ppdevd: src/ppdevd.c
	$(C) $(CFLAGS) -o ppdevd src/ppdevd.c

clean:
	rm -f ppdevd

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -m755 ppdevd ${DESTDIR}${PREFIX}/bin/
	mkdir -p ${DESTDIR}/lib/systemd/system
	install -m644 src/ppdevd.service ${DESTDIR}/lib/systemd/system/

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/ppdevd
	rm -f ${DESTDIR}/lib/systemd/system/ppdevd.service

.PHONY: all clean install uninstall
