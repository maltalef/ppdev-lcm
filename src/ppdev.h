// PPDEV_H
#ifndef PPDEV_H
#define PPDEV_H

#include <stddef.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <linux/ppdev.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include <sys/ioctl.h>
#include <unistd.h>

// Device specific constants
#define ENABLE 0x02
#define INS_L1 0x80
#define INS_L2 0xC0

#define INS_CLEAR 0x01
#define INS_HOME 0x02
#define INS_NOCUR 0x0C
#define INS_UNDRL 0x0E
#define INS_BLKCUR_BLINK 0x0F
#define INS_FSET_8b2l 0x38

#define DataPort 0x378
#define StatusPort 0x379
#define ControlPort 0x37A

#define CUR_NOCUR 0
#define CUR_BLOCK 1
#define CUR_UNDRL 2

#define MSG_SZ 20

int ppdev_init(const char*);
static void ppdev_write_message(int, const char*, int);
void ppdev_close(int);

static void _ioctl(int fd, unsigned long request, char value)
{
	char argp = value;

	if (ioctl(fd, request, &argp) < 0)	{
		fprintf(stderr, "ERROR: error on ioctl(): %s (errno=%d)", strerror(errno), errno);
		exit(1);
	}
}

static void io(int fd, unsigned long request, char value, int sleep)
{
	unsigned char control_seq = 0x0;

     /* NOTE:
      * Register Select (RS) is hardware inverted, that means
      * that when RS is set to high, as far as the parallel port
	  * is concerned, it's set low, and vice-versa.
	 */

	if (request == PPWCONTROL) {
		control_seq |= 0x08; // Sets Register Select (RS) to high (Hardware RS = ~RS)
	}
	_ioctl(fd, PPWDATA, value);

	_ioctl(fd, PPWCONTROL, (control_seq | ENABLE));
	usleep(sleep);
	_ioctl(fd, PPWCONTROL, (control_seq & ~ENABLE));
	usleep(100);
	_ioctl(fd, PPWCONTROL, (control_seq | ENABLE));
	usleep(100);
}

int ppdev_init(const char* path)
{
	// NOTE: Not necessary. Can be removed.
    ioperm(DataPort, ControlPort, 1);

	int fd = open(path, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "ppdev_init() ERROR: Cannot open `%s`: %s (errno=%d)", path, strerror(errno), errno);
        exit(1);
	}

	if (ioctl(fd, PPCLAIM, 0) < 0) {
		fprintf(stderr, "ppdev_init() ERROR: Cannot claim device `%s`: %s (errno=%d)", path, strerror(errno), errno);
        exit(1);
	}

	io(fd, PPWCONTROL, INS_FSET_8b2l, 300); // Function set (8-bit interface, 2 lines, 5*7 Pixels)
	io(fd, PPWCONTROL, 0xf, 300); // display on/off
	io(fd, PPWCONTROL, 0x6, 300); // entry mode set
	io(fd, PPWCONTROL, 0x1, 1800); // clear display
	io(fd, PPWCONTROL, 0x80, 300);

    return fd;
}

void ppdev_write_message(int fd, const char* msg, int ln)
{
    if (strlen(msg) > MSG_SZ) {
        fprintf(stderr, "ppdev_write_message() ERROR: Message size is longer than %d characters.", MSG_SZ);
        exit(1);
    }

    if (ln == 0) {
        io(fd, PPWCONTROL, INS_L1, 400);
        for (size_t i = 0; i < MSG_SZ; i++) {
            io(fd, PPWDATA, msg[i], 140);
        }
    } else if (ln == 1) {
        io(fd, PPWCONTROL, INS_L2, 400);
        for (size_t i = 0; i < MSG_SZ; i++) {
            io(fd, PPWDATA, msg[i], 140);
        }
    }
}

void ppdev_close(int fd)
{
    _ioctl(fd, PPRELEASE, 0);
    if(close(fd) == -1) {
        fprintf(stderr, "ppdev_close() ERROR: Cannot close file descriptor: %s (errno=%d)", strerror(errno), errno);
        exit(1);
    }
}

// PPDEV_H
#endif
