#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <time.h>

#include "ppdev.h"

#define PATH "/dev/parport0"

int main(int _argc, char** argv)
{
	int dev = ppdev_init(PATH);

	char msg_l1[MSG_SZ] = "";
	char msg_l2[MSG_SZ] = "";

	while (1) {
		memset(msg_l1,0, sizeof(msg_l1));
		memset(msg_l2,0, sizeof(msg_l2));

		time_t t = time(NULL);
		struct tm time = *localtime(&t);

		char time_str[6] = "";
		sprintf(time_str, "%2d:%2d", time.tm_hour, time.tm_min);

		struct utsname buf;
		uname(&buf);

		strcat(msg_l1, buf.nodename);

		int dif = MSG_SZ - strlen(buf.nodename) - strlen(time_str);
		for (int i = MSG_SZ - dif; i < MSG_SZ; i++) {
			strcat(msg_l1, " ");
		}

		strcat(msg_l1, time_str);

		struct sysinfo info;
		sysinfo(&info);

		int days = info.uptime / 86400;
		int hours = (info.uptime / 3600) - (days * 24);
		int mins = (info.uptime / 60) - (days * 1440) - (hours * 60);

		sprintf(msg_l2, "%dD %2d:%2d", days, hours, mins);

		char load_str[6] = "";
		double load;
		getloadavg(&load, 1);
		sprintf(load_str, "%2.2f", load);

		dif = MSG_SZ - strlen(msg_l2) - strlen(load_str);

		for (int i = MSG_SZ - dif; i < MSG_SZ; i++) {
			strcat(msg_l2, " ");
		}

		strcat(msg_l2, load_str);

		ppdev_write_message(dev, msg_l1, 0);
		ppdev_write_message(dev, msg_l2, 1);
		sleep(10);
	}

	ppdev_close(dev);

	return 0;
}
